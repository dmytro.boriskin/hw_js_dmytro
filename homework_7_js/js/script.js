// ## Задание
//
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//
//     #### Технические требования:
//     - Создать функцию, которая будет принимать на вход массив.
// - Каждый из элементов массива вывести на страницу в виде пункта списка
// - Необходимо использовать шаблонные строки и функцию `map` массива для формирования контента списка перед выведением его на страницу.

let myArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

// myArray.map(function(item) {
//     let newLi = document.createElement("li");
//     newLi.innerHTML = `${item}`;
//     list.appendChild(newLi);
// });

let mappedArray = myArray.map(function (el){
     return `<li>${el}</li>`;
});
let a = mappedArray.join(" ");

// или через InnerHTML
let ul = document.createElement('ul');
document.body.appendChild(ul);
ul.innerHTML += a;

//или через insertAdjustment
// ul.insertAdjacentHTML( "afterBegin", a);

// Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
let delay = 10;
let counter = document.getElementById('counter');
let timer = setInterval(function() {
        counter.innerHTML = --delay;
        if (!delay) {
            clearInterval(timer);
            document.querySelector("body").style.display='none';
        }
    }, 1000);
counter.innerHTML = delay;
