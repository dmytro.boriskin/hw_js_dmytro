let button = document.createElement("button");
let url = "https://api.ipify.org/?format=json";

button.innerHTML = "Вычислить по IP";
document.body.appendChild(button); 

button.addEventListener("click", function(event) {
   request(url)
        .then((result) => {
        /* console.log(result.ip) */
        request(`http://ip-api.com/json/${result.ip}?lang=ru&fields=continent,country,regionName,city,district`)
        /* console.log(request(`http://ip-api.com/json/${result.ip}?lang=ru&fields=continent,country,regionName,city,district`)) */
            .then((ipresult) => {
                const div = document.createElement("div");
                div.innerHTML =     `<span>IP: ${result.ip}</span><br>
                                     <span>Континент: ${ipresult.continent}</span><br>
                                     <span>Страна: ${ipresult.country}</span><br>
                                     <span> Регион: ${ipresult.regionName}</span><br>
                                     <span>Город: ${ipresult.city}</span><br>
                                     <span>Район: ${ipresult.district}</span>`
                 button.after(div);
       /* console.log(ipresult.continent);
       console.log(ipresult.country);
       console.log(ipresult.regionName);
       console.log(ipresult.city);
       console.log(ipresult.district); */
       })
   })
})

async function request (url) {
        let response = await fetch(url);
        if(!response.ok) {
            console.log("Looks like there was a problem. Status Code: " +  response.status);  
            return;
        } else {
            let reply = await response.json();
            return reply; 
       }
}
