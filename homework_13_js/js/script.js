let switchButton = document.createElement("button");
switchButton.innerHTML = "CHOOSE THEME";
switchButton.style.margin = "20px";
document.getElementById("journey").after(switchButton);

switchButton.onclick = function () {
    if( localStorage.getItem("dark") === null ) {
        document.getElementById("link").href = "css/style2.css";
        localStorage.setItem("dark", "css/style2.css");
    } else {
        document.getElementById("link").href = "css/style.css";
        localStorage.removeItem("dark");
    }
};

let dark = localStorage.getItem("dark");

window.onload = function () {
    if( localStorage.getItem("dark") ) {
        document.getElementById("link").href = dark;
    }
};

// switchButton.onclick = function () {
//     if( localStorage.getItem("color2") === null && localStorage.getItem("color") === null ) {
//         document.getElementById("dark1").style.backgroundColor = "black";
//         document.getElementById("dark2").style.backgroundColor = "black";
//         document.getElementById("dark3").style.backgroundColor = "black";
//         localStorage.setItem("color", "black");
//         document.getElementById("kkk").style.backgroundColor = "red";
//         localStorage.setItem("color2", "red");
//     } else {
//         document.getElementById("kkk").style.backgroundColor = "white";
//         localStorage.removeItem("color2");
//         document.getElementById("dark1").style.backgroundColor = "#305db8";
//         document.getElementById("dark2").style.backgroundColor = "#df4a32";
//         document.getElementById("dark3").style.backgroundColor = "#3cb878";
//         localStorage.removeItem("color");
//     }
// };
//
// let color = localStorage.getItem("color");
// let color2 = localStorage.getItem("color2");
//
// window.onload = function () {
//     if( localStorage.getItem("color") ) {
//         document.getElementById("dark1").style.backgroundColor = color;
//         document.getElementById("dark2").style.backgroundColor = color;
//         document.getElementById("dark3").style.backgroundColor = color;
//     }
//     if( localStorage.getItem("color2") ) {
//         document.getElementById("kkk").style.backgroundColor = color2;
//     }
// };

