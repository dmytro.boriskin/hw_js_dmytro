const tabs = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".tabs-content");
const tabsWrap = document.querySelector(".tabs");
const activeClass = "active";

const tabClick = (() => {
    tabsWrap.addEventListener("click", event => {
        if ( event.target.classList.contains("tabs-title") ) {
            [...tabs].forEach((tab, tabIndex, tabArray) => {
                tab.classList.remove(activeClass);
                contents[tabIndex].classList.remove(activeClass);
                if ( event.target === tab ) {
                    tab.classList.add(activeClass);
                    contents[tabIndex].classList.add(activeClass);
                }
            });
        }
    });
});
tabClick();
