// function factorial () {
//     let factorialNumber = parseInt(prompt("Please enter number > 0"));
//
//     while (isNaN(factorialNumber)) {
//         factorialNumber = parseInt(prompt("Please enter factorial number AGAIN", factorialNumber));
// }
//
//     if(factorialNumber < 0) {
//         alert("Incorrect! Please enter number > 0");
//         factorialNumber = parseInt(prompt("Please enter factorial number AGAIN. Hint: number must be > 0", factorialNumber));
//     }
//
//     let result = 1;
//     for(i=factorialNumber; i > 1; --i)
//     result *=i;
//     return result;
// }
// document.writeln(factorial());

const factorial = (someNumber) => {
    if( someNumber === 1 ) {
        return someNumber;
    } else {
        someNumber *= factorial(someNumber -1);
        return someNumber;
    }
};

let userNumber = parseInt(prompt("Please enter number >0"));

    while (isNaN(userNumber)) {
        userNumber = parseInt(prompt("Please enter factorial number AGAIN", userNumber));
}

    if(userNumber < 0) {
        alert("Incorrect! Please enter number > 0");
        userNumber = parseInt(prompt("Please enter factorial number AGAIN. Hint: number must be > 0", userNumber));
    }

document.writeln(factorial(userNumber));