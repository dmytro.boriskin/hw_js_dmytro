let buttons = document.querySelectorAll("button");

document.onkeypress = function (event) {
    for (let button of buttons) {
        button.style.backgroundColor = "black";
        if( event.key.toLowerCase() === button.innerText.toLowerCase()) {
            button.style.backgroundColor = "blue";
        }
    }
};

// document.onkeypress = function (event) {
//     switch(event.code) {
//     case "Enter":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyS":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyE":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyO":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyN":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyL":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "blue";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "black";
//         break;
//
//     case "KeyZ":
//         document.getElementsByClassName("btn")[0].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[1].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[2].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[3].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[4].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[5].style.backgroundColor = "black";
//         document.getElementsByClassName("btn")[6].style.backgroundColor = "blue";
//         break;
//     }
// };