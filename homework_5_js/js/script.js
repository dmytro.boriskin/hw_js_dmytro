// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем.
//
//     #### Технические требования:
//     - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//     1. При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
// 2. Создать метод `getAge()` который будет возвращать сколько пользователю лет.
// 3. Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.

const createNewUser = () => {
    let newUser = {};
    let nickNameUser = "";
    let age;
    let password = "";

    newUser.firstName = prompt("Please Enter first name", "Dmytro");
    Object.defineProperty( newUser, "firstName", {
        value: newUser.firstName,
        writable: false
    });

    newUser.lastName = prompt("Please Enter last name", "Boriskin");
    Object.defineProperty( newUser, "lastName", {
        value: newUser.lastName,
        writable: false
    });

    newUser.birthday = prompt("Please enter your birthday", "09.11.1986");

    newUser.password = function getPassword() {
       password = this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(this.birthday.length - 4);
       console.log(password);
   };

    newUser.age = function getAge() {
        let year = newUser.birthday.substring(newUser.birthday.length - 4);
        let date = newUser.birthday.slice(0,2);
        let month = newUser.birthday.slice(3,5);
        let dayOfBirth = new Date(year, month, date);
        let dayOfNow = new Date();
        let difference = dayOfNow - dayOfBirth;
        age = Math.floor(difference/31557600000);
        console.log(age);
   };

    newUser.nickName = function getLogin() {
        nickNameUser = this.firstName.slice(0,1).toLowerCase() + this.lastName.toLowerCase();
        console.log(nickNameUser);
    };
    return newUser;
};

const newUser = createNewUser();

newUser.nickName();
newUser.password();
newUser.age();

console.log(newUser);