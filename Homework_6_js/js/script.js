//     ## Задание
//
// Реализовать функцию фильтра массива по указанному типу данных.
//
//     #### Технические требования:
//     - Написать функцию `filterBy()`, которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// - Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

//for
// const filterBy = (array, arg) => {
//    let newArray1 = [];
//      for( let i = 0; i < array.length; i++ ) {
//          if( typeof array[i] !== arg ) {
//             array2.push(array[i])
//         }
//     }
//     return newArray1;
// };
// console.log(filterBy(['hello', 'world', 23, '23', null], "string"));

//forEach
const filterBy = (array, argument)=> {
    let newArray2 = [];
        array.forEach(function(item) {
                if( typeof item !==  argument.toString() ) {
                    newArray2.push(item);
                }
             });
    console.log(newArray2);
};
filterBy(['hello', 'world', 23, '23', null], "object");


// //filter
// const filterBy = (array, argument)=> {
//     let newArray3 = array.filter(function(item) {
//         return typeof item !== argument;
//         });
//     console.log(newArray3);
// };
//
// filterBy(['hello', 'world', 23, '23', null], "string");
