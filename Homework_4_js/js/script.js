// ## Теоретический вопрос
//
// 1. Опишите своими словами, что такое метод обьекта
//
// ## Задание
//
// Реализовать функцию для создания объекта "пользователь".
//
//     #### Технические требования:
//     - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.
//
//     #### Не обязательное задание продвинутой сложности:
//     - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.
//
//     #### Литература:
// - [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
// - [Object.defineProperty()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)


const createNewUser = () => {
        let newUser = {};
        let nickNameUser = "";
            newUser.firstName = prompt("Please Enter first name");
            Object.defineProperty( newUser, "firstName", {
                value: newUser.firstName,
                writable: false
            });

            newUser.lastName = prompt("Please Enter last name");
            Object.defineProperty( newUser, "lastName", {
            value: newUser.lastName,
            writable: false
            });

            newUser.nickName = function getLogin() {
                nickNameUser = this.firstName.slice(0,1).toLowerCase() + this.lastName.toLowerCase();
                console.log(nickNameUser);
            };
    return newUser;
};

const newUser = createNewUser();
newUser.nickName();
console.log(newUser);











