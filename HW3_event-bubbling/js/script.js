function createTable ( tr, td ) {
    const table = document.createElement("table");
    document.body.appendChild(table);
    table.classList.add("table");
    for ( let i = 0; i < tr; i++ ) {
        const tr = document.createElement("tr");
        for (let j = 0; j < td; j++ ) {
            const td = document.createElement("td");
            tr.appendChild(td);
            td.classList.add("row");
        }
        table.appendChild(tr);
    }
}

createTable(30, 30);

document.body.addEventListener("click", function(event) {
    let clickedCell = event.target;
    if ( clickedCell.tagName === "TD" ) {
        clickedCell.classList.toggle("active" )
    }
    if ( clickedCell === event.currentTarget ) {
        document.querySelector(".table" ).classList.toggle("toggle" )
    }
});











