<!--   У вас есть массив строк, каждая строка - один продукт, который входит в состав английского завтрака. Напишите код, который работает так: если в состав завтрака входит больше 4х продуктов, то остальные переносятся на обед.
 -->
const englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];

/* в результате массив englishBreakfast должен быть равен:
englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы"];

и должен появится еще массив dinner, со значениями:
dinner = ["жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
*/

function f(englishBreakfast) {
    if(englishBreakfast.length > 4 ) {
        let dinner = [];
        dinner.push (englishBreakfast.slice(4));
        englishBreakfast.splice(4,8);
        console.log(dinner);
        console.log(englishBreakfast);
    }

}

f(englishBreakfast);