function enterData() {

    let firstNumber = prompt("Please enter 1st number"),
        secondNumber = prompt("Please enter 2nd number");

    while (isNaN(firstNumber) || isNaN(secondNumber) || !firstNumber || !secondNumber) {
        firstNumber = prompt("Please input once again first number", firstNumber);
        secondNumber = prompt("Input input once again second number", secondNumber);
    }

    let sign = prompt("Please enter math operation + - / *");

    while(!'+/-*'.includes(sign) || sign.length !== 1) {
        sign = prompt("Please input once again math operation", sign);
    }

    return {
        first: Number(firstNumber),
        second: Number(secondNumber),
        signOperation: sign
    }
}

let enteredData = enterData();

function calculate(object) {

    let result = 0;

    switch (object.signOperation) {
        case "+": result = object.first+object.second;
            break;
        case "-": result = object.first-object.second;
            break;
        case "/": result = object.first/object.second;
            break;
        case "*": result = object.first*object.second;
            break;
    }
    return result;
}

console.log(calculate(enteredData));