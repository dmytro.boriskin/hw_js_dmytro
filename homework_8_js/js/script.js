// ## Задание
//
// Создать поле для ввода цены с валидацией.
//
//     #### Технические требования:
//     - При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. Это поле будет служить для ввода числовых значений
// - Поведение поля должно быть следующим:
//     - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// - Когда убран фокус с поля - его значение считывается, над полем создается `span`, в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`. Рядом с ним должна быть кнопка с крестиком (`X`). Значение внутри поля ввода окрашивается в зеленый цвет.
// - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
// - В папке `img` лежат примеры реализации поля ввода и создающегося `span`.

let field = document.getElementById("input");
let span = document.createElement("span");
let button = document.createElement("button");
let span2 = document.createElement("span");

field.onfocus = function() {
    field.style.border = `3px solid green`;
    span2.innerHTML = "";
};

field.onblur = function() {
    if( this.value < 0 ) {
        field.style.border = `3px solid red`;
        field.cssText = "text";
        span.innerHTML = "";
        button.remove();
        errorMessage();
    } else {
        field.style.border = ``;
        field.cssText = "color-text-green";
        correctMessage();
    }
};

function correctMessage () {
    span.innerHTML = `Текущая цена: ${field.value}`;
    document.getElementById("form").before(span);


    button.innerText = "x";
    button.style.display = "inline-block";
    button.style.borderRadius = "50%";
    span.after(button);

    button.addEventListener("click", (event) => {
        span.innerHTML = "";
        button.remove();
        field.value = "";
    });
}

function errorMessage () {
    span2.innerText = "Please enter correct price";
    document.getElementById("form").after(span2);

}


